import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.Service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostFormComponent } from './post-form/post-form.component';
import{AngularFireModule} from 'angularfire2';


export const firebaseConfig = {
   apiKey: "AIzaSyBCrZxcBImK5RPLIfRZgwQ8NzIEZ4qTwWM",
    authDomain: "angular-home-ea73f.firebaseapp.com",
    databaseURL: "https://angular-home-ea73f.firebaseio.com",
    storageBucket: "angular-home-ea73f.appspot.com",
    messagingSenderId: "75261192001"
}

const appRoutes:Routes=[
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'',component:UsersComponent},
  {path:'**',component:PageNotFoundComponent}
]



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    UserComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }