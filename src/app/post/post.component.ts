import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
post:Post;

bodyOld;
titleOld;

@Output() deleteEvent = new EventEmitter<Post>();
@Output() editEvent = new EventEmitter<Post>();
@Output() updateEvent = new EventEmitter<Post>();

isEdit:Boolean= false; 
editButtonText = "Edit";


constructor() { }

sendDelete(){
this.deleteEvent.emit(this.post);
}

toggleEdit(){
  this.isEdit = !this.isEdit;
  this.isEdit? this.editButtonText = "save" : this.editButtonText = "Edit";
  this.isEdit? this.saveOldPost() : this.saveNewPost();
  if(!this.isEdit){
    this.updateEvent.emit(this.post);
  }
}

saveOldPost()
{
  this.bodyOld=this.post.body;
  this.titleOld=this.post.title;
}

saveNewPost(){
  this.editEvent.emit(this.post);
}

calcelChanges(){
  this.post.title = this.titleOld;
   this.post.body = this.bodyOld;
   this.isEdit=!this.isEdit;
  this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
 }

  ngOnInit() {
  }

}