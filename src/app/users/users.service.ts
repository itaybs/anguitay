import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {
//private _url = 'http://jsonplaceholder.typicode.com/users';

usersObservabale;

getUsers(){
//return this._http.get(this._url).map(res =>res.json()).delay(2000);
this.usersObservabale = this.af.database.list('/users');
return this.usersObservabale;
  }

constructor(private af:AngularFire) { }

}