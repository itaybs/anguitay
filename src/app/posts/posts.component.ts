import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.Service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
posts;
currentPost;
islodding:Boolean= true;

select(post){

  this.currentPost = post;
  }
constructor(private _postsService:PostsService) { }
addPost(post){
    this._postsService.addPost(post);
   
  }
  
  deletePost(post){
    this._postsService.deletePost(post);
    
  }

editPost(post){
this.posts.splice(
    this.posts.indexOf(post),0
    )
  }

updatePost(post){
    this._postsService.updatePost(post);
  }

  ngOnInit() {
  this._postsService.getPosts().subscribe(postsData =>{  
  this.posts= postsData;
  this.islodding=false;
  })
  console.log(this.posts)});
}

 
}